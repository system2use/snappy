source /etc/lsb-release

alias apt-update="apt-get update"
alias apt-search='aptitude search'
alias apt-install='apt-get install -y --force-yes'
alias apt-remove='apt-get remove -y --force-yes'
alias apt-clean='apt-get -y --force-yes autoremove && apt-get autoclean'
alias apt-upgrade='apt-get -y update && aptitude -y safe-upgrade'

export APT_PKGs="aptitude redis-server"
export APT_PKGs=$APT_PKGs" git-all etckeeper"
export APT_PKGs=$APT_PKGs" wvdial hostapd tor miredo" #i2p
export APT_PKGs=$APT_PKGs" build-essential curl git-all"
