#!/bin/bash

source /shl/bin/env/bash

PPAs="nilarimogard/webupd8 chris-lea/node.js brightbox/ruby-ng i2p-maintainers/i2p resmo/git-ftp"

source /etc/lsb-release

#if [[ $DISTRIB_CODENAME=="precise" ]] ; then
#    PPAs=$PPAs" backbox/three groovy-dev/grails"
#fi

if [[ -e /etc/apt/sources.list ]] ; then
    rm -f /etc/apt/sources.list
fi

touch /etc/apt/sources.list

echo 'deb http://ma.archive.ubuntu.com/ubuntu '$DISTRIB_CODENAME' main restricted universe multiverse' >>/etc/apt/sources.list
echo 'deb-src http://ma.archive.ubuntu.com/ubuntu '$DISTRIB_CODENAME' main restricted universe multiverse' >>/etc/apt/sources.list

echo 'deb http://ma.archive.ubuntu.com/ubuntu '$DISTRIB_CODENAME'-updates main restricted universe multiverse' >>/etc/apt/sources.list
echo 'deb-src http://ma.archive.ubuntu.com/ubuntu '$DISTRIB_CODENAME'-updates main restricted universe multiverse' >>/etc/apt/sources.list

echo 'deb http://ma.archive.ubuntu.com/ubuntu '$DISTRIB_CODENAME'-backports main restricted universe multiverse' >>/etc/apt/sources.list
echo 'deb-src http://ma.archive.ubuntu.com/ubuntu '$DISTRIB_CODENAME'-backports main restricted universe multiverse' >>/etc/apt/sources.list

echo 'deb http://security.ubuntu.com/ubuntu '$DISTRIB_CODENAME'-security main restricted universe multiverse' >>/etc/apt/sources.list
echo 'deb-src http://security.ubuntu.com/ubuntu '$DISTRIB_CODENAME'-security main restricted universe multiverse' >>/etc/apt/sources.list

echo 'deb http://ma.archive.ubuntu.com/ubuntu '$DISTRIB_CODENAME'-proposed restricted main multiverse universe' >>/etc/apt/sources.list
echo 'deb-src http://ma.archive.ubuntu.com/ubuntu '$DISTRIB_CODENAME'-proposed restricted main multiverse universe' >>/etc/apt/sources.list

echo 'deb http://archive.canonical.com/ubuntu '$DISTRIB_CODENAME' partner' >>/etc/apt/sources.list
echo 'deb-src http://archive.canonical.com/ubuntu '$DISTRIB_CODENAME' partner' >>/etc/apt/sources.list

echo 'deb http://extras.ubuntu.com/ubuntu '$DISTRIB_CODENAME' main' >>/etc/apt/sources.list
echo 'deb-src http://extras.ubuntu.com/ubuntu '$DISTRIB_CODENAME' main' >>/etc/apt/sources.list

echo 'deb http://deb.torproject.org/torproject.org '$DISTRIB_CODENAME' main' >>/etc/apt/sources.list

#echo 'deb http://download.virtualbox.org/virtualbox/debian '$DISTRIB_CODENAME' contrib' >>/etc/apt/sources.list

# echo 'deb http://www.apache.org/dist/cassandra/debian 11x main' >>/etc/apt/sources.list
# echo 'deb-src http://www.apache.org/dist/cassandra/debian 11x main' >>/etc/apt/sources.list

echo 'deb http://debian.neo4j.org/repo stable/' >>/etc/apt/sources.list
echo 'deb-src http://debian.neo4j.org/repo stable/' >>/etc/apt/sources.list

#echo 'deb http://ftp.wa.co.za/pub/mariadb/repo/10.0/ubuntu '$DISTRIB_CODENAME' main' >>/etc/apt/sources.list
#echo 'deb-src http://ftp.wa.co.za/pub/mariadb/repo/10.0/ubuntu '$DISTRIB_CODENAME' main' >>/etc/apt/sources.list

wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc -O- | sudo apt-key add -
wget -q http://debian.neo4j.org/neotechnology.gpg.key -O- | apt-key add -

gpg --keyserver keys.gnupg.net --recv 886DDD89
gpg --export A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89 | sudo apt-key add -

apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 16126d3a3e5c1192

for ppa in $PPAs ; do
    add-apt-repository -y ppa:$ppa
done

apt-get update

